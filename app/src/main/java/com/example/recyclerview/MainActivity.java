package com.example.recyclerview;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        LinkedList<Recipe> recipeList = new LinkedList<>();
        recipeList.add(new Recipe("Banana Banana Bread",
                "This banana bread recipe creates the most delicious, moist loaf with loads of banana flavor. Why compromise the banana flavor? Friends and family love my recipe and say it's by far the best! It tastes wonderful toasted. Enjoy!",
                "You likely already have all the ingredients you'll need for this banana bread recipe on hand. If not, here's what to add to your grocery list:\n" +
                        "\n" +
                        "· Flour: All-purpose flour gives the banana bread structure.\n" +
                        "· Baking soda: Baking soda acts as a leavener, which means it helps the banana bread rise.\n" +
                        "· Salt: A pinch of salt enhances the overall flavor, but it won't make the loaf taste salty.\n" +
                        "· Butter: A stick of butter lends richness, moisture, and irresistible flavor.\n" +
                        "· Brown sugar: Brown sugar sweetens things up and adds a hint of warmth.\n" +
                        "· Eggs: Eggs act as a binding agent, which means they hold the batter together.\n" +
                        "· Bananas: Of course, you'll need bananas! Choose overripe bananas.",
                R.drawable.bread));
        recipeList.add(new Recipe("World's Best Lasagna",
                "This lasagna recipe takes a little work, but it is so satisfying and filling that it's worth it!",
                "The Allrecipes community adores this lasagna recipe because it's incredibly customizable, so you can easily alter the ingredient list to suit your needs. If you want to stay true to the original recipe, though, these are the ingredients you'll need to add to your grocery list:\n" +
                        "· Meat: This super meaty lasagna has sweet Italian sausage and lean ground beef.\n" +
                        "· Onion and garlic: An onion and two cloves of garlic are cooked with the meat to add tons of flavor.\n" +
                        "· Tomato products: You'll need a can of crushed tomatoes, two cans of tomato sauce, and two cans of tomato paste.\n" +
                        "· Sugar: Two tablespoons of white sugar add subtle sweetness and enhance the flavor of the sauce.\n" +
                        "· Spices and seasonings: This lasagna recipe is flavored with fresh parsley, dried basil leaves, salt, Italian seasoning, fennel seeds, and black pepper.\n" +
                        "· Lasagna noodles: Use store-bought or homemade lasagna noodles.\n" +
                        "· Cheeses: Parmesan, mozzarella, and ricotta cheese make this lasagna extra decadent.\n" +
                        "· Egg: An egg helps bind the ricotta so it doesn't ooze out of the lasagna when you cut into it.",
                R.drawable.lasagna));

        RecipeAdapter recipeAdapter = new RecipeAdapter((LinkedList<Recipe>) recipeList, this);
        recyclerView.setAdapter(recipeAdapter);
    }
}