package com.example.recyclerview;

public class Recipe {
    private final String title;
    private final String description;
    private final int imageRes;
    private final String details;

    public Recipe(String title, String description, String details, int imageRes) {
        this.title = title;
        this.description = description;
        this.imageRes = imageRes;
        this.details = details;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getImageRes() {
        return imageRes;
    }

    public String getDetails() { return details; }
}
