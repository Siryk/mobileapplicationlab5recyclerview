package com.example.recyclerview;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class RecipeDetailActivity extends AppCompatActivity {

    private TextView recipeTitleTextView;
    private TextView recipeDetailsTextView;
    private ImageView recipeImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_detail_activity);

        initializeViews();
        setupActionBar();

        Intent intent = getIntent();
        if (intent != null) {
            String title = intent.getStringExtra("title");
            String description = intent.getStringExtra("details");
            int imageRes = intent.getIntExtra("imageRes", 0);

            setRecipeDetails(title, description, imageRes);
        }
    }

    private void initializeViews() {
        recipeTitleTextView = findViewById(R.id.recipeTitleTextView);
        recipeDetailsTextView = findViewById(R.id.recipeDetailsTextView);
        recipeImageView = findViewById(R.id.recipeImageView);
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setRecipeDetails(String title, String description, int imageRes) {
        recipeTitleTextView.setText(title);
        recipeDetailsTextView.setText(description);
        recipeImageView.setImageResource(imageRes);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}